#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
#for use on sebio01 or 02 please uncomment the next two lines
#export http_proxy=http://fw-bln:8012
#export ftp_proxy=http://fw-bln:8012

infile=${1} #list of sra numbers in unix linebreak formatted file
resultfolder=${2} 
cwd=`pwd`

mkdir -p ${resultfolder}
cd ${resultfolder}
for i in `cat ${infile}`; do
	if [ -f ${i}.sam.gz ]; then continue; fi;
        prefix=${i:0:3}
        short=${i:0:6}
        wget ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/${prefix}/${short}/${i}/${i}.sra -O ${i}.sra
	fastq-dump ${i}.sra
	
done
cd ${cwd}
